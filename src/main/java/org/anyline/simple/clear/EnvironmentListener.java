package org.anyline.simple.clear;

import org.noear.solon.Solon;
import org.noear.solon.annotation.Component;
import org.noear.solon.core.event.AppLoadEndEvent;
import org.noear.solon.core.event.EventListener;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

@Component
public class EnvironmentListener implements EventListener<AppLoadEndEvent> {
    @Override
    public void onEvent(AppLoadEndEvent event) {
        configs();
        datasource();
    }

    /**
     * 加载配置文件数据源
     */
    public void datasource(){

    }
    /**
     * 配置文件 赋值 ConfigTable
     */
    public void configs(){
        Field[] fields = ConfigTable.class.getDeclaredFields();
        for(Field field:fields){
            String name = field.getName();

        }
    }
}
