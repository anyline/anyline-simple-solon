package org.anyline.simple.clear.controller;

import org.anyline.simple.clear.ConfigTable;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.Context;

@Controller
@Mapping("usr")
public class UserController {
    @Inject(required=false)
    private ConfigTable configTable;
    @Mapping("/list")
    public String list(){
        Context ctx = Context.current();
        return "list1";
    }
}
